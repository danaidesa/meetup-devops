#### Demo Yml 

#### Setup
- Crear proyecto
- Agregar secret deploy key para que pueda pullear repositorio.
- Agregar Image Stream
- Agregar Build Config
- Agregar Deployment Config
- Agregar Service
- Agregar Ruta

```shell
oc new-project demo-yml
oc create secret generic deploy-key --from-file=ssh-privatekey=.ssh/deploy_key
oc apply -f is.yml
oc apply -f bc.yml
oc apply -f deployment.yml
oc apply -f service.yml
oc apply -f route.yml
```