### Setup de minishift

#### Up:
- Hyper-V
- Descargar minishift y agregar al path
- Agregar Hyper-V external switch
- 
    ```shell
    minishift setup
    minishift addons install --defaults
    minishift config set memory 8GB
    minishift config set cpus 4
    minishift config set disk-size 50GB
    minishift config set image-caching true
    minishift addons enable admin-user
    minishift addons enable registry-route
    # minishift start --hyperv-virtual-switch "Minishift Eth"
    minishift start --hyperv-virtual-switch "Minishift"
    ```

#### Down: 
- `minishift stop`
- `minishift delete`