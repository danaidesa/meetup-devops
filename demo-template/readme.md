### Demo template

- Crear proyecto
- Crear deployment con template en github.
- Ver el docker build
- Publicar servicio
- Acceder a ruta.

#### En nuestro servidor minishift/okd

```shell
oc new-project demo-s2i
oc new-app https://github.com/sclorg/nodejs-ex -l name=node-ex
oc logs -f bc/nodejs-ex
oc expose svc/nodejs-ex
minishift openshift service nodejs-ex --in-browser
```